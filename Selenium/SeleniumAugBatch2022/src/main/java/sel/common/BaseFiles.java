package sel.common;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class BaseFiles {

	public static WebDriver driver;
	public static WebDriverWait wait;

	@BeforeMethod
	public void start() {
		System.out.println("Start Method");
	}

	@AfterMethod
	public void tearDown() {
		System.out.println("End Method");
	}

	@BeforeSuite
	public void startSuite() {
		System.out.println("Start Report");
	}

	@AfterSuite
	public void endSuite() {
		System.out.println("End Report");
	}

	@BeforeClass
	public void startClass() {
		System.out.println("Start Browser");
		startBrowser("chrome");
	}

	@AfterClass
	public void endClass() {
		System.out.println("End Browser");
		quitDriver();
	}

	public void startBrowser(String browser) {

		try {
			switch (browser.toLowerCase().trim()) {

			case "chrome":
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
				break;
			case "firefox":
				System.setProperty("webdriver.gecko.driver", "<path of gecko driver>");
				driver = new FirefoxDriver();
				break;

			case "ie":
				System.setProperty("webdriver.ie.driver", "<path of ie driver>");
				driver = new InternetExplorerDriver();
				break;

			default:
				System.out.println("Please pass correct brrowser");

			}
		} catch (Exception e) {
			System.err.println("Browser could not be launched");
			e.printStackTrace();
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

	}

	public void passURL(String url) {
		try {
			if (url.isEmpty() || url == null) {
				System.out.println("URL is empty or null");
			} else if (!url.contains("http") || !url.contains("https")) {
				System.out.println("URL doesn't contain http/https");
			} else
				driver.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public By getBy(String locType, String Locator) {

		try {
			switch (locType) {

			case "id":
				return By.id(Locator);
			case "name":
				return By.name(Locator);
			case "xpath":
				return By.xpath(Locator);
			case "cssselector":
				return By.cssSelector(Locator);
			case "linktext":
				return By.linkText(Locator);
			case "partiallinktext":
				return By.partialLinkText(Locator);
			case "classname":
				return By.className(Locator);
			case "tagname":
				return By.tagName(Locator);
			default:
				System.out.println("Incorrect locator");

			}

			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public WebElement getWebElement(By by) {
		try {
			return driver.findElement(by);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void typeKeys(WebElement ele, String value) {
		try {
			ele.clear();
			ele.sendKeys(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitDriver() {
		try {
			driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isPresent(By by) {
		try {
			if (driver.findElement(by).isDisplayed())
				return true;
			else
				return false;
		} catch (Exception e) {
			return false;
		}
	}

}
