package sel.week9.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class GoogleSuggestion extends BaseFiles {

	By search = getBy("xpath", "//input[@title='Search']");

	@Test
	public void SuggestionList() throws InterruptedException {
		passURL("https://www.google.com/");
		typeKeys(getWebElement(search), "apple");
		Thread.sleep(2000);
		List<WebElement> searchlist = driver.findElements(By.xpath("//li[@role='presentation']"));
		
		for (WebElement list : searchlist) {
			String gList = list.getText();
			if (gList.toLowerCase().contains("iphone 13")) {
				click(list);
				break;
			} 
			System.out.println(gList);
		}
		// driver.findElement(By.xpath("//b[contains(text(),'iphone 13')]")).click();
		Thread.sleep(2000);
	}

}
