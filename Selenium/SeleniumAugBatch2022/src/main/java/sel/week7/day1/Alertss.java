package sel.week7.day1;

import org.openqa.selenium.Alert;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class Alertss extends BaseFiles{
	
	@Test
	public void handleAlert() throws InterruptedException {
		
		startBrowser("chrome");
		passURL("https://the-internet.herokuapp.com/javascript_alerts");
		
		getWebElement(getBy("xpath", "//button[text()='Click for JS Alert']")).click();
		Thread.sleep(3000);
		
		Alert alert = driver.switchTo().alert();
		
		alert.accept();
		

		
		getWebElement(getBy("xpath", "//button[text()='Click for JS Confirm']")).click();
		Thread.sleep(3000);

		alert.dismiss();
		
		getWebElement(getBy("xpath", "//button[text()='Click for JS Prompt']")).click();
		
		Thread.sleep(3000);
		driver.switchTo().alert().sendKeys("Selenium");
		
		String text = driver.switchTo().alert().getText();
		
		System.out.println(text);

	}
	

}
