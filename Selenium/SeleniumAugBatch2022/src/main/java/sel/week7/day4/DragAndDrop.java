package sel.week7.day4;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class DragAndDrop extends BaseFiles{
	
	@Test
	public void hoverAction() throws InterruptedException {
		
		startBrowser("chrome");
		passURL("https://jqueryui.com/droppable/");
		
		driver.switchTo().frame(getWebElement(getBy("classname", "demo-frame")));

		
		WebElement draggable = getWebElement(getBy("id", "draggable"));
		WebElement droppable = getWebElement(getBy("id", "droppable"));
		
		
		
		Actions act = new Actions(driver);
		act.clickAndHold(draggable).moveToElement(droppable).build().perform();
		
	

		
		
		
		
	}

}
