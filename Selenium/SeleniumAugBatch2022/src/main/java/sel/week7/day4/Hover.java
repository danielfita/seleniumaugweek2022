package sel.week7.day4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class Hover extends BaseFiles{
	
	
	
	@Test(enabled=false)
	public void hoverAction() throws InterruptedException {
		
		startBrowser("chrome");
		passURL("https://www.amazon.in/");
		
		By byAccount = getBy("id", "nav-link-accountList");
		By byYourAccount = getBy("xpath","//a/*[text()='Your Account']");
		
		WebElement eleAccount = getWebElement(byAccount);
		WebElement eleYourAccount = getWebElement(byYourAccount);
		
		Actions builder = new Actions(driver);
		builder.moveToElement(eleAccount).perform();
		
		Thread.sleep(2000);
		
		eleYourAccount.click();
		
		
	}
	
	@Test
	public void mrBoolHover() {
		
		startBrowser("chrome");
		
		driver.get("http://mrbool.com/");
//		passURL("http://mrbool.com/");
		
		By byContent = getBy("classname", "menulink");
		WebElement eleContent = getWebElement(byContent);
		
		Actions builder = new Actions(driver);
		builder.moveToElement(eleContent).perform();
		
		By byCourses = getBy("linktext", "COURSES");
		click(getWebElement(byCourses));
		
		
	}

}
