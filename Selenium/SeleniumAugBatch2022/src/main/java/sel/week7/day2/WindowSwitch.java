package sel.week7.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class WindowSwitch extends BaseFiles{
	
	@Test
	public void winSwitch()
	{
		startBrowser("chrome");
		passURL("https://the-internet.herokuapp.com/windows");
		
		getWebElement(getBy("linktext", "Click Here")).click();
		
		String parentWindow = driver.getWindowHandle();
		
		System.out.println(parentWindow);
		
		Set<String> windowHandles = driver.getWindowHandles();
		
		System.out.println(windowHandles);
		
		List<String> li = new ArrayList<>();
		
		li.addAll(windowHandles);
		
//		driver.switchTo().window(windowHandles.toArray()[1].toString());
		
		driver.switchTo().window(li.get(1));
		
		String childWindow = driver.getWindowHandle();
		
		System.out.println(childWindow);

		driver.switchTo().window(parentWindow);
		
		System.out.println(driver.getWindowHandle());
		
		
	}

}
