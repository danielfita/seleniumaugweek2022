package sel.week7.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class Frames extends BaseFiles{
	
	@Test
	public void frame()
	{
		startBrowser("chrome");
		passURL("https://the-internet.herokuapp.com/nested_frames");
		
		driver.switchTo().frame("frame-top");
		driver.switchTo().frame("frame-left");
		System.out.println(driver.findElement(By.xpath("/html/body")).getText());
		
		
		driver.switchTo().parentFrame();
		driver.switchTo().frame("frame-middle");
		System.out.println(driver.findElement(By.xpath("/html/body")).getText());

		driver.switchTo().parentFrame();
		driver.switchTo().frame("frame-right");
		System.out.println(driver.findElement(By.xpath("/html/body")).getText());

		driver.switchTo().defaultContent();
		driver.switchTo().frame("frame-bottom");
		System.out.println(driver.findElement(By.xpath("/html/body")).getText());
		
		
				
				 
		
		
	}
	

}
