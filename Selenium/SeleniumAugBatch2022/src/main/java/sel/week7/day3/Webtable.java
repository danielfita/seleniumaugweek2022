package sel.week7.day3;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class Webtable extends BaseFiles {

	private List<WebElement> rows;

	@Test
	public void table() {
		startBrowser("chrome");
		passURL("https://www.techlistic.com/p/demo-selenium-practice.html");
		rows = driver.findElements(By.xpath("//table[@id='customers']//tr"));
		
		for (WebElement row : rows) {
			List<WebElement> data =row.findElements(By.tagName("td"));
			for (WebElement datum : data) {
				System.out.print(datum.getText());
				System.out.print("\t");
			}System.out.println("");
		}
	}
}
