package sel.week5.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import sel.common.BaseFiles;



public class DropDown extends BaseFiles{
	
	@Test
	public void ddSelect() {
		
		startBrowser("chrome");
		passURL("https://www.amazon.in/");
		
		WebElement searchDD = driver.findElement(By.id("searchDropdownBox"));
		
		Select dd = new Select(searchDD);
		dd.selectByVisibleText("Books");
		
		List<WebElement> ddOptions = dd.getOptions();
		
		for(int i=0; i<ddOptions.size();i++) {
			System.out.println(ddOptions.get(i).getText());
		}
		
		
	}


}
