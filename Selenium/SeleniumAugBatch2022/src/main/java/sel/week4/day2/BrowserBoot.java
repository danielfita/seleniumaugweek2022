package sel.week4.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserBoot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.amazon.in/");
		
		String amazonTitle = driver.getTitle();
		String expectedTitle = "Online Shopping  in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in";
		
		System.out.println(amazonTitle);
		
		if(amazonTitle.equals(expectedTitle))
			
		{
			System.out.println("Title is as expected");
		}
		else 
		{
			System.err.println("actual titile is "+amazonTitle+" Expected title is "+expectedTitle+" Hence Failed");
		}
		
		driver.quit();

	}

}
