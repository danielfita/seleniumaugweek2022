package sel.week8.day4;

import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;

import amazon.pages.HomePage;
import amazon.pages.LoginPage;
import sel.common.BaseFiles;

public class AmazonTest extends BaseFiles {

	HomePage hp = new HomePage();
	LoginPage lp = new LoginPage();

	@Test(priority = 1)
	public void login() throws InterruptedException {

		passURL("https://www.amazon.in/");
		hp.clickSignIn();
		lp.enterUserName("ljhgsdlhbdkjklj");
		String userNameValue = lp.getUserNameValue();
		assertEquals(userNameValue, "ljhgsdlhbdkjklj");

	}

	@Test(priority = 2)
	public void incorrectLogingVerify() throws InterruptedException {

		lp.clickContinue();
		assertEquals(lp.isIncorrectAccountErrorDisplayed(), true);
	}

}
