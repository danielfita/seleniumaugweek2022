package sel.week8.day1;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class Pagination extends BaseFiles {
	
	static By byHK;

	@Test
	public void pages() throws InterruptedException {

		startBrowser("Chrome");
		passURL("https://selectorshub.com/xpath-practice-page/");
		byHK = By.xpath("//table[@id='tablepress-1']//td[text()='Hong Kong']");
		By byNext =By.xpath("//a[@id='tablepress-1_next']");
		
		Thread.sleep(5000);

		while (true) {

			
			if (isPresent(byHK)) {
				System.out.println("Success");
				break;
			} else {
				boolean isNextDisabled = driver.findElement(byNext).getAttribute("class").contains("disable");
				if (!isNextDisabled) {
					driver.findElement(byNext).click();
				} else {
					System.out.println("Elment is not found in the table");
					break;
				}
			}

		}

	}

}
