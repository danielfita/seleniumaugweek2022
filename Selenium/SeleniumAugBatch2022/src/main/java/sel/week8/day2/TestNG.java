package sel.week8.day2;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class TestNG extends BaseFiles{
	
	@BeforeTest
	public void beforeTest()
	{
		System.out.println("Test Data1");
	}
	
	
	@Test
	public void method1()
	{
		System.out.println("Method 1");
	}
	
	@Test
	public void method2()
	{
		System.out.println("Method 2");
	}
	
	@Test
	public void method3()
	{
		System.out.println("Method 3");
	}

}
