package sel.week8.day2;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class ExplicitWait extends BaseFiles {
	
	static By byHK;
	WebDriverWait wait;

	@Test
	public void pages() throws InterruptedException {

		
		startBrowser("Chrome");

		passURL("https://selectorshub.com/xpath-practice-page/");
		byHK = By.xpath("//table[@id='tablepress-1']//td[text()='Hong Kong']");
		By byNext =By.xpath("//a[@id='tablepress-1_next']");
		
		wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.invisibilityOf(driver.findElement(byHK)));

		while (true) {

			
			if (isPresent(byHK)) {
				System.out.println("Success");
				break;
			} else {
				boolean isNextDisabled = driver.findElement(byNext).getAttribute("class").contains("disable");
				if (!isNextDisabled) {
					driver.findElement(byNext).click();
				} else {
					System.out.println("Elment is not found in the table");
					break;
				}
			}

		}

	}


}
