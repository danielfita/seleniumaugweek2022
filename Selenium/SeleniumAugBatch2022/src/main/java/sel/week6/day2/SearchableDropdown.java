package sel.week6.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import sel.common.BaseFiles;

public class SearchableDropdown extends BaseFiles{
	
	
	@Test
	public void dd() {
		
		startBrowser("chrome");
		passURL("https://www.google.com/");
		
		getWebElement(getBy("name", "q")).sendKeys("testing");
		
		List<WebElement> listOfSuggestions = driver.findElements(getBy("xpath", "//ul[@role='listbox']/li"));
		
		for(WebElement eachSuggestions :listOfSuggestions)
		{
			String str = eachSuggestions.getText();
//			System.out.println(str);
			
			if(str.toLowerCase().contains("courses"))
			{
				eachSuggestions.click();
				break;
			}
		}

		
	}
	
	

}
