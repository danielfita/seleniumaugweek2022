package sel.week6.day1;

public class Xpath {
	
	//Standard xpath with attribute
	//  - //tagname[@attribute='value']
	//link - https://www.opencart.com/index.php?route=account/login
	//xpath - //input[@name='email']
		
	
	//Standard xpath with text
	//  - //tagname[text()='value']
	//link - https://www.techlistic.com/p/demo-selenium-practice.html
	// copied xpath of google - //*[@id="customers"]/tbody/tr[2]/td[1]
	// Custom xpath - //td[text()='Google']
			
	
	//Xpath - to child
	//link - https://www.techlistic.com/p/demo-selenium-practice.html
	// // - points to indirect element
	// / - points to direct elements
	
	//xpath - //table[@id='customers']/tbody/tr/th
	
	//Xpath to parent
	//link - https://www.techlistic.com/p/demo-selenium-practice.html
	//xpath - //table[@id='customers']//tr/..
	//xpath - //table[@id='customers']//tr/parent::tbody
	
	//Xpath with following-sibling
	//link - https://www.techlistic.com/p/demo-selenium-practice.html
	// <point to the element>//following-sibling::tagname
	//xpath - //td[text()='Google']//following-sibling::td
	
	
	//Xpath with preceding-sibling
	// <point to the element>//preceding-sibling::tagname
	//xpath - //td[text()='Maria Anders']//preceding-sibling::td
	
	//Xpath with following
	
	//xpath - //td[text()='Microsoft']//following::td
	
	//Xpath with preceding
	
	//xpath - //td[text()='Microsoft']//preceding::td

	
	
	//Xpath with ancestor
	// <point to the element>//ancestor::tagname
	//td[text()='Germany']/ancestor::tbody



	
	

}
