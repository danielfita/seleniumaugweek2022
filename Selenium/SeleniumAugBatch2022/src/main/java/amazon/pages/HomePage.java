package amazon.pages;

import org.openqa.selenium.By;

import sel.common.BaseFiles;

public class HomePage extends BaseFiles {

	By bySignIn = getBy("id", "nav-link-accountList-nav-line-1");

	public void clickSignIn() {
		click(getWebElement(bySignIn));
	}

}
