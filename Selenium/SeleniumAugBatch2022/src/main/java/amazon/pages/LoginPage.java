package amazon.pages;

import org.openqa.selenium.By;

import sel.common.BaseFiles;

public class LoginPage extends BaseFiles {

	By byAmazonUN = getBy("id", "ap_email");
	By incorrectAccountError = getBy("xpath","//span[contains(text(),'We cannot find an account with that email address')]");
	By byContinueButton = getBy("id", "continue");


	public void enterUserName(String data) {
		typeKeys(getWebElement(byAmazonUN), data);
	}

	public String getUserNameValue() {
		return getWebElement(byAmazonUN).getAttribute("value");
	}
	
	public void clickContinue() {
		click(getWebElement(byContinueButton));
	}
	
	public boolean isIncorrectAccountErrorDisplayed() {
		return isPresent(incorrectAccountError);
	}

}
