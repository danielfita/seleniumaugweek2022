package com.week2.day2;

public class Employee {

	String empName;
	int empNo;
	String city;
	char gender;
	boolean isActive;

	public static void main(String[] args) {
		
		
		Employee emp1 =new Employee();
		Employee emp2 =new Employee();
		Employee emp3 =new Employee();

		emp1.empName="Parkavi";
		emp1.empNo=1;
		emp1.city="Chennai";
		emp1.gender='F';
		emp1.isActive=true;
		
		System.out.println(emp1.empName+"  "+emp1.empNo+" "+emp1.city+"  ");
		System.out.println(emp2.empName+"  "+emp2.empNo+" "+emp2.city+"  ");
		emp3=null;
		//emp3.empName="Kowsalya";//Null pointer Exception
		//System.out.println(emp3.empName);
//		emp1.method1();
		
		new Employee().empName = "Ravi";
			
		
	}
	
	void method1()
	{
		empName="Abc";
		
		System.out.println(empName);
		
	}

}
