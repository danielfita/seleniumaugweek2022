package com.week2.day1;

import java.util.ArrayList;

public class DynamicArray {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		@SuppressWarnings("rawtypes")
		ArrayList empInfo = new ArrayList();
		
		empInfo.add("Ram"); //0
		empInfo.add(234); // 1
		empInfo.add(2000000.00); //2
		empInfo.add(true); //3
		empInfo.add('M'); //4
		
		System.out.println(empInfo);
		System.out.println(empInfo.size());
		
		
		System.out.println("-------------------------------");
		
		for(Object eachEmp:empInfo)
		{
			System.out.println(eachEmp);
		}
		
		System.out.println("-------------------------------");
		
		empInfo.add("Leela");//5
		System.out.println(empInfo.size());
		
		empInfo.remove(3);
		System.out.println(empInfo);
		System.out.println(empInfo.size());




		
		
	}

}
