package com.week2.day1;

public class StaticArray {

	public static void main(String[] args) {

		int[] nums = new int[4];

		nums[0] = 23;
		nums[1] = 76;
		nums[2] = 876;
		nums[3] = 6564;

//		num[-1] = 798; ArrayIndexOutOfBoundsException
//		nums[4] = 10; //ArrayIndexOutOfBoundsException

//		System.out.println(nums); it prints the address

		for (int i = 0; i < 4; i++) {
			System.out.println(nums[i]);
		}

		int[] numbers = { 23, 89, 647, 824, 90 };

		int lowerIndex = 0;
		int higherIndex = numbers.length - 1;

		for (int number : numbers) {
			System.out.println(number);
		}

	}

}
