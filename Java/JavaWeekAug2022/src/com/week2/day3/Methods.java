package com.week2.day3;

public class Methods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 2, b = 3;
		String names;
		Methods m1 = new Methods();
		m1.method1();
		m1.method2("hello");
		m1.sum(a, b);
		names = m1.getName();
		m1.Multiply(3, 3);
		System.out.println(names);
		System.out.println(m1.Multiply(3, 3));

	}

	// No arguments No return type

	public void method1() {
		System.out.println("hi");
		method2("hi all");
	}

	// no return type with arg

	public void method2(String s1) {
		System.out.println(s1);
	}

	public void sum(int a, int b) {
		System.out.println(a + b);
	}

	// no arg with return type

	public String getName() {
		String name = "Kowsalya";
		return name;
	}

	// with argu with return type

	public int Multiply(int a, int b) {

		int c = a * b;
		return c;
	}
}
