package com.week2.day4;

public class ConstructorSessions {

	String name;

	public ConstructorSessions() {

		System.out.println("I'm inside constructor");

	}

	public ConstructorSessions(String name) {
		this.name = name;
		System.out.println("I'm inside paramatrized constructor "+this.name);

	}

	public static void main(String[] args) {

		ConstructorSessions cs = new ConstructorSessions();
//		cs.name = "Chan";
		System.out.println(cs.name);
	}

}
