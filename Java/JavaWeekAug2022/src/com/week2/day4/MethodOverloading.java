package com.week2.day4;

public class MethodOverloading {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MethodOverloading mo = new  MethodOverloading();
		mo.testMathod(7,5);
		mo.testMathod("Test Variable");

	}
	
	public void testMathod() {
		System.out.println("I'm in testMathod");
	}
	
	public void testMathod(int a) {
		System.out.println("I'm in testMathod "+a);
	}
	
	public void testMathod(int a, int b) {
		System.out.println("I'm in testMathod "+(a+b));
	}
	
	public void testMathod(String var) {
		System.out.println(var);
	}
	

}
