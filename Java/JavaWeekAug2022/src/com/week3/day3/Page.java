package com.week3.day3;

public abstract class Page {
	
	public abstract void title();
	
	public abstract void url();
	
	public void timeOut() {
		System.out.println("Page - timeOut");
	}
	
	public void checkLogo() {
		System.out.println("Page - logo");
	}


}
