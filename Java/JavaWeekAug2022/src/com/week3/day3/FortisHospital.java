package com.week3.day3;

public class FortisHospital extends Medical implements USMedical, UKMedical, IndianMedical {

	@Override
	public void neuroServices() {

		System.out.println("FH - neuroServices");
	}

	@Override
	public void pediaServices() {
		System.out.println("FH - pediaServices");

	}

	@Override
	public void physioServices() {
		System.out.println("FH - physioServices");

	}

	@Override
	public void orthoServices() {
		System.out.println("FH - orthoServices");

	}

	@Override
	public void oncologyServices() {
		System.out.println("FH - oncologyServices");

	}

	@Override
	public void entServices() {
		System.out.println("FH - entServices");

	}

	public void optServices() {
		System.out.println("FH - optServices");

	}

	// method hiding
	public static void billing() {

		System.out.println("FH - billing");

	}

	@Override
	public void covid19Vaccination() {
		System.out.println("FH - covid19Vaccination");
		
	}

}
