package com.week3.day3;

public class HomePage extends Page{

	@Override
	public void title() {
		System.out.println("HP - title");
		
	}

	@Override
	public void url() {
		System.out.println("HP - url");
		
	}
	
	public void message() {
		System.out.println("HP - message");
		
	}

}
