package com.week3.day3;

public class TestPage {

	public static void main(String[] args) {

		LoginPage lp = new LoginPage();
		lp.title();
		lp.url();
		lp.checkLogo();
		lp.doLogin();
		lp.timeOut();
		
		System.out.println("-----------------------");
		
		HomePage hp = new HomePage();
		hp.title();
		hp.url();
		hp.checkLogo();
		hp.timeOut();
		hp.message();
		
		System.out.println("-----------------------");

		// top casting
		Page p = new HomePage();
		p.title();
		p.url();
		p.timeOut();
		p.checkLogo();
	
	}

}
