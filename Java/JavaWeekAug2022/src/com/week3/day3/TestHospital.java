package com.week3.day3;

public class TestHospital {

	public static void main(String[] args) {
		
		FortisHospital fh = new FortisHospital();
		fh.entServices();
		fh.neuroServices();
		fh.pediaServices();
		fh.physioServices();
		fh.orthoServices();
		fh.oncologyServices();
		fh.optServices();
		fh.medicalTraining();
		fh.covid19Vaccination();
		
		FortisHospital.billing();
		USMedical.billing();
		
//		USMedical usm = new USMedical(); we can't create object for interface
		
		// top casting
		USMedical us = new FortisHospital();
		
		us.neuroServices();
		us.pediaServices();
		us.medicalTraining();
		
		fh.medicalEquipment();
		fh.equipmentMaintenance();
		
	}

}
