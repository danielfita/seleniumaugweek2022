package com.week3.day3;

public interface USMedical extends WHO{
	
	public void neuroServices();
	
	public void pediaServices();
	
	//after java 1.8 static method and default method can contain method body in Interfaces
	
	public static void billing() {
		
		System.out.println("US - billing");
		
	}
	
	default void medicalTraining() {
		
		System.out.println("US - medicalTraining");
	}
	
	



}
