package com.week3.day3;

public class Medical {
	
	public void medicalEquipment() {
		System.out.println("Medical - medicalEquipment");
	}
	
	public void equipmentMaintenance() {
		System.out.println("Medical - equipmentMaintenance");
	}

}
