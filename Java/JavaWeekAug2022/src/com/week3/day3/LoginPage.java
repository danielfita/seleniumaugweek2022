package com.week3.day3;

public class LoginPage extends Page {

	@Override
	public void title() {
		System.out.println("LP - title");
	}

	@Override
	public void url() {
		System.out.println("LP - url");

	}
	
	public void doLogin() {
		System.out.println("LP - doLogin");
	}

}
