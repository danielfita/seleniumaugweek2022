package com.week3.day2;

public class TestCar {

	public static void main(String[] args) {

		BMW bmw = new BMW();
		bmw.start();
		bmw.stop();
		bmw.refuel();
		bmw.autoTransmission();
		bmw.engine();
		
		System.out.println("--------------------------------------");
		
		Car car = new Car();
		car.start();
		car.stop();
		car.refuel();
		car.engine();
		
		System.out.println("--------------------------------------");

		//top Casting
		Car car1 = new BMW();
		car1.start();
		car1.stop();
		car1.refuel();
		car1.engine();
	
		System.out.println("--------------------------------------");

		//down casting
//		BMW bmw1 = (BMW) new Car();
//		bmw1.start();
		
		BMW.tyre();
		Car.tyre();

		
	}

}
