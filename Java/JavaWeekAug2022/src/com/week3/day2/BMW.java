package com.week3.day2;

public class BMW extends Car{
	
	
	public void start() {
		System.out.println("BMW - Start");
	}
	
	public void autoTransmission() {
		System.out.println("BMW - autoTransmission");
	}
	
	
	//method hiding
	public static void tyre() {
		System.out.println("BMW - Tyre");
	}
	
	

}
