package com.week3.day2;

public class Car extends Vehicle{
	
	public void start() {
		System.out.println("Car - Start");
	}
	
	public void stop() {
		System.out.println("Car - stop");
	}
	
	public void refuel() {
		System.out.println("Car - refuel");
	}
	
	public static void tyre() {
		System.out.println("Car - Tyre");
	}
	
	

}
