package com.week3.day1;

public class StringManipulation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "This is my Java class";

		int len = str.length();
		System.out.println(len); // 21

		char ch = str.charAt(5);
		System.out.println(ch); // i

//		str.charAt(-1); // java.lang.StringIndexOutOfBoundsException
//		str.charAt(21);// java.lang.StringIndexOutOfBoundsException

		int indexOfJ = str.indexOf("J");
		System.out.println(indexOfJ); // 11

		int indexOfJava = str.indexOf("Java");
		System.out.println(indexOfJava); // 11

		int indexOfS = str.indexOf("Welcome");
		System.out.println(indexOfS); // -1
		
		int indexOfs = str.indexOf("s");
		System.out.println(indexOfs); // 3
		
		int indexOfSecondS = str.indexOf("s",str.indexOf("s")+1);
		System.out.println(indexOfSecondS); // 6
		
		String st1 = "Hi Google";
		String st2 = "Hi google";
		
		System.out.println(st1.equals(st2)); // false
		System.out.println(st1.equalsIgnoreCase(st2)); // true
		
		System.out.println(st1==st2);
		
		String st3 = new String("Kowsalya");
		String st4 = new String("Kowsalya");
		
		System.out.println(st3.equals(st4)); //true
		System.out.println(st3==st4); //false
		
		String st5 = "Daniel";
		String st6 = "Daniel";
		
		System.out.println(st5.equals(st6)); // true
		System.out.println(st5==st6); // true
		
		System.out.println(st1.toUpperCase());
		System.out.println(st1.toLowerCase());
		
		String st7 = "            hi selenium                        ";
		System.out.println(st7.trim().replace(" ", ""));
		
		String st8 = "Your order numbere is 100768";
		System.out.println(st8.substring(st8.indexOf("is")+3,st8.length()));
		
		

		
		





		


		
		

	}

}
