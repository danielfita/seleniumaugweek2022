package com.week4.day1;

import java.util.ArrayList;

public class ExceptionHandling {

	public static void main(String[] args) {

		int a = 1;

		int b = 6;

		ArrayList al = new ArrayList();

		al = null;

		try {
			b = 9 / a;
			System.out.println(b);
			al.add("Babu");
			System.out.println(al);

		} 
		catch(Exception e)
		{
			System.out.println("Handled Exception");
//			e.printStackTrace();


		}
//		catch (ArithmeticException e) {
//			
//			System.out.println("Handled ArithmeticException");
//			e.printStackTrace();
//			
//		} catch (NullPointerException e) {
//			
//			System.out.println("Handled NullPointerException");
//			e.printStackTrace();
//
//		} 
		finally {
			System.out.println("I'm from finally block");
		}

		System.out.println("out of danger");
	}

}
