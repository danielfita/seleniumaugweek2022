package com.week1.day4;

public class SwitchCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String browser = "CHROME ";
		switch (browser.toLowerCase().trim()) {
		case "chrome":
			System.out.println("Launch Chrome");
			break;
		case "firefox":
			System.out.println("Launch Firefox");
			break;
		case "ie":
			System.out.println("Launch ie");
			break;
		case "opera":
			System.out.println("Launch opera");
			break;
		case "safari":
			System.out.println("Launch safari");
			break;
		default:
			System.out.println("please input valid browser name");
		}

	}

}
