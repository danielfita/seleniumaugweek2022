package com.week1.day2;

public class IncrementOrDecrement {

	public static void main(String[] args) {
		
		// post increment
		// a++ = a=a+1
		
		int a = 1;
		a++;
		System.out.println(a++);
		System.out.println(a);
		
		System.out.println("***************************");

		//pre Increment
		int b = 1;
		++b;
		System.out.println(++b);
		System.out.println(b);
		
		System.out.println("***************************");
		
		// post decrement
				// a-- = a=a-1
				
				int c = 10;
				c--;
				System.out.println(c--);
				System.out.println(c);
				
				System.out.println("***************************");

				//pre decrement
				int d = 10;
				--d;
				System.out.println(--d);
				System.out.println(d);
				
				System.out.println("***************************");
		
//		int c = 1;
//		int d = 2;
//		c--;
//		d=--c;
//		System.out.println(--c);
//		System.out.println(d);

	}

}
