package com.week1.day3;

public class IfAndIfElse {

	public static void main(String[] args) {

		// if
		// if else
		// if else if
		// nested if

		// if else
		int a = 40;
		if (a > 50) {
			System.out.println("a is greater than 50");
		} else {
			System.out.println("a is lesser than 50");
		}

		System.out.println("*********");
		// if else if

		int x = 200;
		int y = 3000;
		int z = 400;
		if (x > y && x > z) {
			System.out.println("x is grater");
		} else if (y > z) {
			System.out.println("y is grater");
		} else {
			System.out.println("z is grater ");
		}
	}
}
