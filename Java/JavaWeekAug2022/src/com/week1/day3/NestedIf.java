package com.week1.day3;

public class NestedIf {

	public static void main(String[] args) {

		int mark = 96;

		if (mark >= 90)

		{

			if (mark > 95) {
				System.out.println("A+ grade");
			} else {
				System.out.println("A grade");
			}

		} else {
			System.out.println("B grade");
		}
	}
}
