package com.week1.day1;

public class Datatypes {

	public static void main(String[] args) {

		//DataTypes
		//1. this create a memory for the variable
		//types
		//1. primitive data types
			//1. Boolean
				//1. boolean - true/false
			//2.Numeric
				//1. Character - char
				//2. Integral - byte, short, int, long
				//3.Floating point - float, double
		//2. Non- primitive
			// String, class, Arrays, Interface
		
		//Syntax - <datatype> <space> <VariableName> = <value>;
		//Boolean
		//size - 1 bit
		boolean flag = true;
		System.out.println(flag);
		
		//char
		//size - 2 byte
		char ch= 'a';
		System.out.println(ch);
		
		//byte
				//size - 1 byte
				//range - -128 to 127
				byte weekNum = 127;
				System.out.println(weekNum);
				
				//short
				//size - 2 byte
				//range -32*** to +32***
				short a = 32000;
				System.out.println(a);
				
				//int
				//size - 4 bytes
				int b = 765432;
				System.out.println(b);
				
				//long
				//size - 8 byte
				long worldPopulation = 7654908643578866l;
				System.out.println(worldPopulation);
				
				//float
				//size - 4 bytes
				float pi = 3.14f;
				System.out.println(pi);
				
				//double
				//size - 8 bytes
				double sunDistance = 98765332689.12;
				System.out.println(sunDistance);
				
		
	}

}
